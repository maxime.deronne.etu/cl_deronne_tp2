import org.springframework.context.support.ClassPathXmlApplicationContext;
import trois_tiers.Metier;
import trois_tiers.Presentation;
import trois_tiers.Stockage;

public class ApplicationTp {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext c =  new ClassPathXmlApplicationContext("trois_tiers/configuration.xml");
        Metier metier = c.getBean("metier", Metier.class);
        Presentation presentation = c.getBean("presentation", Presentation.class);
        Stockage stockage = c.getBean("stockage", Stockage.class);
        Metier metierFabrique = c.getBean("metier_fabrique", Metier.class);
        Presentation presentationFabrique = c.getBean("presentation_fabrique", Presentation.class);
        Stockage stockageFabrique = c.getBean("stockage_fabrique", Stockage.class);
        Metier metierSingleton = c.getBean("metier_singleton", Metier.class);
        Presentation presentationSingleton = c.getBean("presentation_singleton", Presentation.class);
        Stockage stockageSingleton = c.getBean("stockage_singleton", Stockage.class);
        Metier metierPrototype = c.getBean("metier_prototype", Metier.class);
        Presentation presentationPrototype = c.getBean("presentation_prototype", Presentation.class);
        Stockage stockagePrototype = c.getBean("stockage_prototype", Stockage.class);
        Metier metierInitDestruct = c.getBean("metier_init_destroy", Metier.class);
        Presentation presentationInitDestruct = c.getBean("presentation_init_destroy", Presentation.class);
        Stockage stockageInitDestruct = c.getBean("stockage_init_destroy", Stockage.class);
        Metier metierLazy = c.getBean("metier_lazy", Metier.class);
        Presentation presentationLazy = c.getBean("presentation_lazy", Presentation.class);
        Stockage stockageLazy = c.getBean("stockage_lazy", Stockage.class);

        c.close();
    }
}
