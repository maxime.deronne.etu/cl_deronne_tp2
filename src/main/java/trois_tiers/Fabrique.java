package trois_tiers;

public class Fabrique {

    public Metier creerMetier(){
        return new Metier(new Stockage());
    }

    public Presentation creerPresentation(){
        return new Presentation();
    }

    public Stockage creerStockage(){
        return new Stockage();
    }
}
